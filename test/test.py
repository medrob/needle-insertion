import Sofa
import os

class MainScene(Sofa.PythonScriptController):

    def createGraph(self, node):

        self.rootNode = node.getRoot()
        self.rootNode.dt = 0.02
        self.rootNode.gravity = [0, -9.81, 0]

        self.rootNode.createObject('RequiredPlugin', name='SofaMiscCollision')
        self.rootNode.createObject('RequiredPlugin', name='SofaPython')
        self.rootNode.createObject('RequiredPlugin', name='CImgPlugin')
        self.rootNode.createObject('RequiredPlugin', name='SofaOpenglVisual')

        # Maybe display flags should be an array
        self.rootNode.createObject('VisualStyle', displayFlags='showBehavior showCollisionModels')
        self.rootNode.createObject('FreeMotionAnimationLoop', name='Animation')
        self.rootNode.createObject('DefaultPipeline', name='CollisionPipeline')
        self.rootNode.createObject('BruteForceDetection', name='BruteForceDetection')
        self.rootNode.createObject('LocalMinDistance', name='Proximity', alarmDistance=0.2, contactDistance=0.09, angleCone=0.0)
        self.rootNode.createObject('DefaultContactManager', name='Response', response='FrictionContact')
        self.rootNode.createObject('DefaultCollisionGroupManager')
        self.rootNode.createObject('LCPConstraintSolver', maxIt=1000, tolerance=0.001)

        #------
        # Fat node
        #------

        fat_meshFile = '../data/phantom/Segmentation_Mar09_Fat.stl'
        
        fatNode = self.rootNode.createChild('Fat')

        fatNode.createObject('EulerImplicitSolver', rayleighStiffness=0.1, rayleighMass=0.1)
        fatNode.createObject('CGLinearSolver', iterations=25, tolerance=1e-5, threshold=1e-5)
        fatNode.createObject('MechanicalObject', name='FatRigid', template='Rigid3d', dy=300)
        fatNode.createObject('UniformMass', totalMass=10.0)
        fatNode.createObject('UncoupledConstraintCorrection')

        #fat_visu = fatNode.createChild('FatVisual')
        
        #fat_visu.createObject('MeshSTLLoader', name='fatVisuMeshLoader', filename = fat_meshFile)
        #fat_visu.createObject('OglModel', name='FatVisu', src='@fatVisuMeshLoader')
        #fat_visu.createObject('RigidMapping', input='@../FatRigid', output='@FatVisu')

        fat_coll = fatNode.createChild('FatCollis')

        fat_coll.createObject('MeshSTLLoader', name='fatCollMeshLoader', filename = fat_meshFile)
        fat_coll.createObject('MeshTopology', name='FatMesh', src='@fatCollMeshLoader')
        fat_coll.createObject('MechanicalObject', name='FatColl')
        fat_coll.createObject('TriangleCollisionModel')
        fat_coll.createObject('LineCollisionModel')
        fat_coll.createObject('PointCollisionModel')
        fat_coll.createObject('RigidMapping', input='@../FatRigid', output='@FatColl')

        #------
        # Syringe node
        #------

        syringe_meshFile = '../data/syrette2.obj'
        
        siryngeNode = self.rootNode.createChild('Syringe')

        siryngeNode.createObject('EulerImplicitSolver', rayleighStiffness=0.1, rayleighMass=0.1)
        siryngeNode.createObject('CGLinearSolver', iterations=25, tolerance=1e-5, threshold=1e-5)
        siryngeNode.createObject('MechanicalObject', name='SyringeRigid', template='Rigid3d', dy=400, scale3d=[10, 10, 10])
        siryngeNode.createObject('UniformMass', totalMass=1.0)
        siryngeNode.createObject('UncoupledConstraintCorrection')

        #syringe_visu = siryngeNode.createChild('SyringeVisual')
        
        #syringe_visu.createObject('MeshSTLLoader', name='syringeVisuMeshLoader', filename = syringe_meshFile)
        #syringe_visu.createObject('OglModel', name='SyringeVisu', src='@syringeVisuMeshLoader')
        #syringe_visu.createObject('RigidMapping', input='@../SyringeRigid', output='@SyringeVisu')

        syringe_coll = siryngeNode.createChild('SyringeCollis')

        syringe_coll.createObject('MeshObjLoader', name='syringeCollMeshLoader', filename = syringe_meshFile)
        syringe_coll.createObject('MeshTopology', name='SyringeMesh', src='@syringeCollMeshLoader')
        syringe_coll.createObject('MechanicalObject', name='SyringeColl', scale3d=[10, 10, 10])
        syringe_coll.createObject('TriangleCollisionModel')
        syringe_coll.createObject('LineCollisionModel')
        syringe_coll.createObject('PointCollisionModel')
        syringe_coll.createObject('RigidMapping', input='@../SyringeRigid', output='@SyringeColl')

        #------
        #Floor
        #------

        floor_meshFile = '../data/floor.obj'

        floorNode = self.rootNode.createChild('Floor')

        floorNode.createObject('MeshObjLoader', filename=floor_meshFile, name='floorMeshLoader')
        floorNode.createObject('MeshTopology', src='@floorMeshLoader')
        floorNode.createObject('MechanicalObject', scale3d=[5, 5, 5])
        floorNode.createObject('TriangleCollisionModel')
        floorNode.createObject('LineCollisionModel')
        floorNode.createObject('PointCollisionModel')
        
        return 0



def createScene(rootNode):
    obj = MainScene(rootNode)
    obj.createGraph(rootNode)