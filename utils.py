import numpy as np

def str2np(H_str):
    H = list()
    for line in H_str.strip().split('\n'):
        vec = list()
        for num in line.strip().split(' '):
            try:
                vec.append(float(num))
            except: # ""
                continue
        H.append(vec)
    return np.array(H)
