import Sofa
from components import Fat, Muscle, Liver, Needle
from utils import str2np
import numpy as np
import matplotlib.pyplot as plt

class MainScene(Sofa.PythonScriptController):

    def createGraph(self, node):

        # displayFlags = ['showForceFields', 'showCollisionModels', 'showBehavior']
        displayFlags = []

        self.rootNode = node.getRoot()
        self.rootNode.dt = 0.005  # In seconds
        self.rootNode.gravity = [0, 0, 0]

        self.rootNode.createObject('RequiredPlugin', name='SofaMiscCollision')
        self.rootNode.createObject('RequiredPlugin', name='SofaPython')
        self.rootNode.createObject('RequiredPlugin', name='CImgPlugin')
        self.rootNode.createObject('RequiredPlugin', name='SofaOpenglVisual')
        self.rootNode.createObject('RequiredPlugin', name='SofaValidation')

        self.rootNode.createObject('VisualStyle', displayFlags=displayFlags)

        self.rootNode.createObject('FreeMotionAnimationLoop')
        self.rootNode.createObject('GenericConstraintSolver', name='constraintSolver', maxIterations="25", tolerance="1e-07", computeConstraintForces="true")
        self.rootNode.createObject('DefaultPipeline', verbose="0", depth="6", draw="1")
        self.rootNode.createObject('BruteForceDetection', name='N2')
        self.rootNode.createObject('LocalMinDistance', name='Intersection',
                                   alarmDistance="0.7", contactDistance="0.5", useLMDFilters="0")
        self.rootNode.createObject('DefaultContactManager', name='Response', response='FrictionContact',
                                   responseParams='mu=0.8')

        self.iteration = [0]
        self.ax = plt.axes()
        self.fatForce = [0]
        self.muscleForce = [0]
        self.liverForce = [0]
        
        # Add needle's node
        Needle(self.rootNode)

        # Add phantom's nodes
        Fat(self.rootNode)              # Fat node
        Muscle(self.rootNode)           # Muscle Node
        Liver(self.rootNode)            # Liver

        # Other phantom's nodes not used:
        # Kidneys
        #Kidney(self.rootNode, meshFile_kidney='data/phantom/Segmentation_Mar09_Kidney_L.stl', name='kidney_left')
        #Kidney(self.rootNode, meshFile_kidney='data/phantom/Segmentation_Mar09_Kidney_R.stl', name='kidney_right')
        # Ribs
        #for i in range(1, 7):
        #    Rib(self.rootNode, meshFile_rib='data/phantom/Segmentation_Mar09_Rib_'+str(i)+'.stl', name='rib_'+str(i))
        # Aorta
        #Aorta(self.rootNode)
        # Inner Spine
        #InnerSpine(self.rootNode)
        # Outer Spine
        #OuterSpine(self.rootNode)
        # Vena Cava
        #VenaCava(self.rootNode)
        # Partial Lung
        #PartialLung(self.rootNode)
        # Partial Vein
        #PortalVein(self.rootNode)

        return 0

    def onEndAnimationStep(self, dt):
        # Prints matrix H which should represent the direction of the collision
        # source: https://www.sofa-framework.org/community/forum/topic/pneumatic-actuator-soft-robots-plugin/
        fatForce = np.linalg.norm(self.rootNode.getChild('Fat').getObject('mechObject').force)
        muscleForce = np.linalg.norm(self.rootNode.getChild('Muscle').getObject('mechObject').force)
        liverForce = np.linalg.norm(self.rootNode.getChild('liver').getObject('mechObject').force)

        self.fatForce.append(fatForce)
        self.muscleForce.append(muscleForce)
        self.liverForce.append(liverForce)

        self.iteration.append(self.iteration[-1]+0.005)
        self.ax.plot(self.iteration, self.fatForce, 'r-')
        self.ax.plot(self.iteration, self.muscleForce, 'b-')
        self.ax.plot(self.iteration, self.liverForce, 'g-')
        plt.draw()
        plt.pause(0.0001)
        return 0
