class VenaCava:

    def define_solvers(self):
        self.venaCavaNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.venaCavaNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.venaCavaNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.venaCavaNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.venaCavaNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.venaCavaNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.45", youngModulus="450")

        self.venaCavaNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(7):
                for j in range(10):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.venaCavaNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        venaCavaVisNode = self.venaCavaNode.createChild('Visual')

        venaCavaVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_venaCava)
        venaCavaVisNode.createObject('OglModel', src='@meshLoader', color='#0000ff', name='muscle')
        venaCavaVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        venaCavaColNode = self.venaCavaNode.createChild('Collision')

        venaCavaColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_venaCava)
        venaCavaColNode.createObject('MeshTopology', src='@meshLoader')
        venaCavaColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        venaCavaColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        venaCavaColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # venaCavaColNode.createObject('LineCollisionModel')
        # venaCavaColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        pass

    def __init__(self, rootNode, meshFile_venaCava='data/phantom/Segmentation_Mar09_Vena_Cava.stl'):
        self.meshFile_venaCava = meshFile_venaCava
        self.venaCavaNode = rootNode.createChild('venaCava')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








