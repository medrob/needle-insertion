class Fat:

    def define_solvers(self):
        self.fatNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.fatNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.fatNode.createObject('MechanicalObject', name='mechObject', template='Vec3d',
                             dx="0", dy="0", dz="0", rx="0", ry="0", rz="0", scale="1.0")
        self.fatNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="10")

    def define_topology_fem(self):
        self.fatNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-155", xmax="125", ymin="-170",
                             ymax="45", zmin="-120", zmax="10")

        # fatNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.fatNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                             poissonRatio="0.48", youngModulus="500")

        self.fatNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ""
        for k in range(10):
            for i in range(10):
                for j in range(8):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.fatNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        fatVisNode = self.fatNode.createChild('Visual')

        fatVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_fat)
        fatVisNode.createObject('OglModel', src='@meshLoader', color='#ecc854', name='Fat')
        fatVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@Fat')

    def define_collision(self):
        # Collision node

        fatColNode = self.fatNode.createChild('Collision')

        fatColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_fat)
        fatColNode.createObject('MeshTopology', src='@meshLoader')
        fatColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject', template='Vec3d',
                                scale="1.0")
        fatColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        fatColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # fatColNode.createObject('LineCollisionModel')
        # fatColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        ################# MONITOR #################
        self.fatNode.createObject('Monitor', name='monitor-fat', indices = "449 459 549 559", template='Vec3d',
                             listening=1, showPositions=False, PositionsColor='1 0 1 1', ExportPositions=False, showVelocities=False, VelocitiesColor='0.5 0.5 1 1',
                             ExportVelocities=False, showForces=False, ForcesColor='0.8 0.2 0.2 1', ExportForces=True, showTrajectories=False, TrajectoriesPrecision="0.1",
                             TrajectoriesColor='0 1 1 1', sizeFactor="0.1")

    def __init__(self, rootNode, meshFile_fat = 'data/phantom/Segmentation_Mar09_Fat.stl'):
        self.meshFile_fat = meshFile_fat
        self.fatNode = rootNode.createChild('Fat')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








