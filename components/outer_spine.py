class OuterSpine:

    def define_solvers(self):
        self.outerSpineNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.outerSpineNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.outerSpineNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.outerSpineNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.outerSpineNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.outerSpineNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.45", youngModulus="450")

        self.outerSpineNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(7):
                for j in range(10):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.outerSpineNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        outerSpineVisNode = self.outerSpineNode.createChild('Visual')

        outerSpineVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_outerSpine)
        outerSpineVisNode.createObject('OglModel', src='@meshLoader', color='#b8a284', name='muscle')
        outerSpineVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        outerSpineColNode = self.outerSpineNode.createChild('Collision')

        outerSpineColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_outerSpine)
        outerSpineColNode.createObject('MeshTopology', src='@meshLoader')
        outerSpineColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        outerSpineColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        outerSpineColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # outerSpineColNode.createObject('LineCollisionModel')
        # outerSpineColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        pass

    def __init__(self, rootNode, meshFile_outerSpine='data/phantom/Segmentation_Mar09_Outer_spine.stl'):
        self.meshFile_outerSpine = meshFile_outerSpine
        self.outerSpineNode = rootNode.createChild('outerSpine')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








