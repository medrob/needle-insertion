class Liver:

    def define_solvers(self):
        self.liverNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.liverNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.liverNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.liverNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.liverNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.liverNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.3", youngModulus="1000")

        self.liverNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(10):
                for j in range(8):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.liverNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        liverVisNode = self.liverNode.createChild('Visual')

        liverVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_liver)
        liverVisNode.createObject('OglModel', src='@meshLoader', color='#844243', name='muscle')
        liverVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        liverColNode = self.liverNode.createChild('Collision')

        liverColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_liver)
        liverColNode.createObject('MeshTopology', src='@meshLoader')
        liverColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        liverColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        liverColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # liverColNode.createObject('LineCollisionModel')
        # liverColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        self.liverNode.createObject('Monitor', name='monitor-liver', indices = "449 459 549 559", template='Vec3d',
                             listening=1, showPositions=False, PositionsColor='1 0 1 1', ExportPositions=False, showVelocities=False, VelocitiesColor='0.5 0.5 1 1',
                             ExportVelocities=False, showForces=False, ForcesColor='0.8 0.2 0.2 1', ExportForces=True, showTrajectories=False, TrajectoriesPrecision="0.1",
                             TrajectoriesColor='0 1 1 1', sizeFactor="0.1")


    def __init__(self, rootNode, meshFile_liver='data/phantom/Segmentation_Mar09_Liver.stl'):
        self.meshFile_liver = meshFile_liver
        self.liverNode = rootNode.createChild('liver')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








