class Needle:

    def define_solvers(self):
        self.needleNode.createObject('EulerImplicitSolver', name='ODE solver', rayleighStiffness="0.01", rayleighMass="1.0")
        self.needleNode.createObject('CGLinearSolver', name='linear solver', iterations="25", tolerance="1e-7", threshold="1e-7", printLog=False)

    def define_mechanics(self):
        self.needleNode.createObject('MechanicalObject', name='mechObject', template='Rigid3d',
                                     dx="240", dy="-60", dz="-60", rx="0", ry="180", rz="0.0", scale3d=[1.2*self.scale, self.scale, self.scale])
        self.needleNode.createObject('UniformMass', name='mass', totalMass="1")
        self.needleNode.createObject('UncoupledConstraintCorrection')

    def define_topology_fem(self):
        pass

    def define_visual(self):
        # Visual node
        needleVisNode = self.needleNode.createChild('VisualModel')

        needleVisNode.createObject('MeshObjLoader', name='instrumentMeshLoader', filename=self.meshFile_needle)
        needleVisNode.createObject('OglModel', name='InstrumentVisualModel', src='@instrumentMeshLoader',
                                   dy="0", scale3d=[1.2 * self.scale, self.scale, self.scale])
        needleVisNode.createObject('RigidMapping', name='MM-VM mapping', input='@../mechObject',
                                   output='@InstrumentVisualModel')

    def define_collision(self):
        # Collision node
        needleColNode = self.needleNode.createChild('CollisionModel')

        needleColNode.createObject('MeshObjLoader', filename=self.meshFile_needle, name='loader')
        needleColNode.createObject('MeshTopology', src='@loader', name='InstrumentCollisionModel')
        needleColNode.createObject('MechanicalObject', src='@InstrumentCollisionModel', name='instrumentCollisionState',
                                   dy="0", scale3d=[1.2 * self.scale, self.scale, self.scale])
        needleColNode.createObject('RigidMapping', name='MM-CM mapping', input='@../mechObject',
                                   output='@instrumentCollisionState')

        # needleColNode.createObject('TriangleCollisionModel', name='instrumentTriangle', contactStiffness=10, contactFriction=10)
        # needleColNode.createObject('LineCollisionModel', name='instrumentLine', contactStiffness=10, contactFriction=10)
        needleColNode.createObject('PointCollisionModel', name='instrumentPoint', contactStiffness="10",
                                   contactFriction="10")

    def define_monitor(self):
        pass

    def define_linear_constraint(self):
        self.needleNode.createObject('LinearMovementConstraint', template='Rigid3d',
                      indices=0, keyTimes=[0, 0.8, 1.7, 1.8],
                      movements=[[0, 0, 0, 0, 0, 0],
                                 [-85, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0],
                                 [0, 0, 0, 0, 0, 0]])


    def __init__(self, rootNode, meshFile_needle = 'data/plug.obj', scale=4):
        self.meshFile_needle = meshFile_needle
        self.needleNode = rootNode.createChild('Needle')
        self.scale = scale

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        #self.define_monitor()
        # Linear movement
        self.define_linear_constraint()








