class PortalVein:

    def define_solvers(self):
        self.portalVeinNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.portalVeinNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.portalVeinNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.portalVeinNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.portalVeinNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.portalVeinNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.45", youngModulus="450")

        self.portalVeinNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(7):
                for j in range(10):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.portalVeinNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        portalVeinVisNode = self.portalVeinNode.createChild('Visual')

        portalVeinVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_portalVein)
        portalVeinVisNode.createObject('OglModel', src='@meshLoader', color='#red', name='muscle')
        portalVeinVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        portalVeinColNode = self.portalVeinNode.createChild('Collision')

        portalVeinColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_portalVein)
        portalVeinColNode.createObject('MeshTopology', src='@meshLoader')
        portalVeinColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        portalVeinColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        portalVeinColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # portalVeinColNode.createObject('LineCollisionModel')
        # portalVeinColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        pass

    def __init__(self, rootNode, meshFile_portalVein='data/phantom/Segmentation_Mar09_Portal_Vein.stl'):
        self.meshFile_portalVein = meshFile_portalVein
        self.portalVeinNode = rootNode.createChild('portalVein')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








