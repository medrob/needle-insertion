class Muscle:

    def define_solvers(self):
        self.muscleNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.muscleNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.muscleNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.muscleNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.muscleNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.muscleNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.45", youngModulus="450")

        self.muscleNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(10):
                for j in range(8):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.muscleNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        muscleVisNode = self.muscleNode.createChild('Visual')

        muscleVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_muscle)
        muscleVisNode.createObject('OglModel', src='@meshLoader', color='#db8063', name='muscle')
        muscleVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        muscleColNode = self.muscleNode.createChild('Collision')

        muscleColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_muscle)
        muscleColNode.createObject('MeshTopology', src='@meshLoader')
        muscleColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        muscleColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        muscleColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # muscleColNode.createObject('LineCollisionModel')
        # muscleColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        self.muscleNode.createObject('Monitor', name='monitor-muscle', indices = "449 459 549 559", template='Vec3d',
                             listening=1, showPositions=False, PositionsColor='1 0 1 1', ExportPositions=False, showVelocities=False, VelocitiesColor='0.5 0.5 1 1',
                             ExportVelocities=False, showForces=False, ForcesColor='0.8 0.2 0.2 1', ExportForces=True, showTrajectories=False, TrajectoriesPrecision="0.1",
                             TrajectoriesColor='0 1 1 1', sizeFactor="0.1")



    def __init__(self, rootNode, meshFile_muscle = 'data/phantom/Segmentation_Mar09_muscle.stl'):
        self.meshFile_muscle = meshFile_muscle
        self.muscleNode = rootNode.createChild('Muscle')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








