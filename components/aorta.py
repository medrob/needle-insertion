class Aorta:

    def define_solvers(self):
        self.aortaNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
        self.aortaNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=False)

    def define_mechanics(self):
        self.aortaNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0",
                                     rx="0", ry="0", rz="0", scale="0.99")
        self.aortaNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")

    def define_topology_fem(self):
        self.aortaNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120",
                                     ymin="-155", ymax="33", zmin="-115", zmax="5")

        # muscleColNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue',
        # 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False,
        # 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

        self.aortaNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large',
                                     poissonRatio="0.45", youngModulus="450")

        self.aortaNode.createObject('UncoupledConstraintCorrection')

        ## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
        indices = ''
        for k in range(10):
            for i in range(7):
                for j in range(10):
                    idx = k * 100 + i * 10 + j
                    indices += str(idx) + " "
        self.aortaNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

    def define_visual(self):
        # Visual node
        aortaVisNode = self.aortaNode.createChild('Visual')

        aortaVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_aorta)
        aortaVisNode.createObject('OglModel', src='@meshLoader', color='#ff0000', name='muscle')
        aortaVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@muscle')

    def define_collision(self):
        # Collision node
        aortaColNode = self.aortaNode.createChild('Collision')

        aortaColNode.createObject('MeshSTLLoader', name='meshLoader', filename=self.meshFile_aorta)
        aortaColNode.createObject('MeshTopology', src='@meshLoader')
        aortaColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject',
                                   template='Vec3d', scale="1.0")
        aortaColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

        aortaColNode.createObject('TriangleCollisionModel', template='Vec3d')
        # aortaColNode.createObject('LineCollisionModel')
        # aortaColNode.createObject('PointCollisionModel')

    def define_monitor(self):
        pass

    def __init__(self, rootNode, meshFile_aorta='data/phantom/Segmentation_Mar09_Aorta.stl'):
        self.meshFile_aorta = meshFile_aorta
        self.aortaNode = rootNode.createChild('Aorta')

        # Solvers
        self.define_solvers()
        # Mechanics
        self.define_mechanics()
        # Topology and force field
        self.define_topology_fem()
        # Visual node
        self.define_visual()
        # Collision node
        self.define_collision()
        # Monitor
        self.define_monitor()








