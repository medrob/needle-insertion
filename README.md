# Needle Insertion - SOFA

## Simulation Environment

### Plugins

Plugins are needed to use some features of SOFA, we use the followings:

* SofaMiscCollision
* SofaPython
* CImgPlugin
* SofaOpenglVisual
* SofaValidation

### AnimationLoop

All the scenes in SOFA must include an AnimationLoop component (if one is not specified, DefaultAnimationLoop is used) which is needed to define the order of all the steps of the simulation and the system resolution, defining a step() function. More details can be found [here](https://www.sofa-framework.org/community/doc/simulation-principles/animation-loop/).

We use a FreeMotionAnimation Loop which works like specified [here](https://www.sofa-framework.org/community/doc/components/animationloop/freemotionanimationloop/).
This component require the definition of both a ConstraintSolver and a ConstraintCorrection, which are shown below

### ConstraintSolver

SOFA allows the use of Lagrange multipliers to handle complex constraints. In order to handle constraint problems we need to define a ConstraintSolver which will be called by the step() function. The solveConstraint() function of the ConstraintSolver organizes and rules all the steps of the resolution of the constraint problem. More details [here](https://www.sofa-framework.org/community/doc/simulation-principles/constraint/lagrange-constraint/#constraintsolver-in-sofa).

There are 2 ConstraintSolver implementations in SOFA:

1. GenericConstraintSolver
2. LCPConstraintSolver

We use the first one as it is more general, its documentation can be found [here](https://www.sofa-framework.org/api/master/sofa/html/classsofa_1_1component_1_1constraintset_1_1_generic_constraint_solver.html).

Used parameters:

* maxIt: maximum number of iterations of the solving algorithm (Gauss-Seidel algorithm);
* tolerance: threshold of residual error to terminate the solving algorithm;
* allVerified: whether all constraints must be verified (constraint i error < tolerance) or not.

### Pipeline

Pipeline component defines the sequence of Collision models and controls the sequence of computations. We use DefaultPipeline ([doc](https://www.sofa-framework.org/api/master/sofa/html/classsofa_1_1component_1_1collision_1_1_default_pipeline.html)).

### Collision Management

SOFA handles collisions in 2 phases ([details](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/collisions/#collision-detection)):

1. **Broad** phase: in which a set of possibly colliding pairs of points is collected;
2. **Narrow** phase: in which it elaborates on that set to determine those which are actually colliding.

This separation is used to save computational resources, discarding as soon as possible clearly not colliding points.

#### Broad collision detection

The ForceDetection component is the one which is in charge of carrying out the **broad** phase. The 4 available ones are described [here](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/collisions/#collision-detection).  
We use the BruteForceDetection ([doc](https://www.sofa-framework.org/api/master/sofa/html/classsofa_1_1component_1_1collision_1_1_brute_force_detection.html)) which performs n^2 pairwise computations and works reliably
More details [here](https://www.sofa-framework.org/community/doc/components/collision/detection-brute-force/).

#### Narrow collision detection

The **narrow** phase is handled by an intersection method ([details](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/collisions/#intersection-methods)).  
We use the LocalMinDistance method.  
Used parameters:

* alarmDistance: Maximum distance for which a collision between 2 elements is created;
* contactDistance: Parameter used in the contact reaction.

### Contact Manager

We use DefaultContactManager to handle the consequences of collisions. In particular we model collisions with friction with mu = 10.

## Objects

Every object will be defined in a node ([details](https://www.sofa-framework.org/community/doc/simulation-principles/scene-graph/)).

### Integration scheme

There are [several](https://www.sofa-framework.org/community/doc/simulation-principles/system-resolution/integration-schemes/) possible options to define the integration scheme to be used.  
We use the EulerImplicitSolver ([doc](https://www.sofa-framework.org/api/master/sofa/html/classsofa_1_1component_1_1odesolver_1_1_euler_implicit_solver.html)), details [here](https://www.sofa-framework.org/community/doc/components/integrationscheme/eulerimplicitsolver/).  
Used parameters:

* rayleighStiffness: Rayleigh damping coefficient related to stiffness - basically the higher the longer the body will take to change its velocity when the acceleration changes sign, proportionately to the stiffness of the object;
* rayleighMass: Rayleigh damping coefficient related to mass.

### Linear Solver

In order to solve the system (Ax=b) defined by the integration scheme we need to define a linear solver.  
Our choice is CGLinearSolver ([doc](https://www.sofa-framework.org/api/master/sofa/html/classsofa_1_1component_1_1linearsolver_1_1_c_g_linear_solver.html)), [details](https://www.sofa-framework.org/community/doc/components/linearsolver/cglinearsolver/). This solver tries to solve the system iteratively.  
Used parameters:

* iterations: maximum number of iterations after which the iterative descent of the CGLinearSolver must stop;
* tolerance: defines the desired accuracy of the Conjugate Gradient solution (ratio of current residual norm over initial residual norm);
* threshold: defines the minimum value of the denominator in the conjugate Gradient solution;

### Mechanical Object

The MechanicalObject component ([details](https://www.sofa-framework.org/community/doc/simulation-principles/mechanicalobject/)) must be specified for every object and defines its mechanical component.  
Used parameters:

* template: data structure;
* dx, dy, dz: position of the body;
* rx, ry, rz: orientation of the body.

### Mass

Every object has a mass associated to it which is represented by an inertia matrix M.  
While there are various options, we use the UniformMass ([details](https://www.sofa-framework.org/community/doc/components/masses/uniformmass/)) component which assumes the M matrix to be purely diagonal.

* template: data structure;
* totalMass: M = eye(n)*totalMass/n.

### Topology

We need to define a topology component to represent the mechanical object in space.  
We use RegularGridTopology with the following parameters:

* nx, ny, nz: number of cubes per axis;
* xmin, xmax, ymin, ymax , zmin, zmax = cube's dimensions.

### ForceField

In order to simulate the elastic behavior we use a FEMForceField ([details](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/physics-integration/#fem-at-a-glance)).  
We use HexahedronFEMForceField because it works on objects with 6 faces which is the best default approximation for our phantom.  
Used parameters:

* method
* poissonRatio
* youngModulus
* computeVonMisesStress
* showVonMisesStressPerNode
* listening
* updateStiffness
* showStressColorMap
* isCompliance

### Constraint Correction

We need to define a constraint correction ([details](https://www.sofa-framework.org/community/doc/simulation-principles/constraint/lagrange-constraint/#constraintcorrection)) component to define how the compliace matrix W should be computed.  
We use UncoupledConstraintCorrection, assuming a purely diagonal (i.e. constraints independent from the others) matrix W.

### Visual Node

#### MeshLoader

There is one mesh loader for every 3d model extension (e.g. stl, obj, ...).

#### OglModel

OglModel is used to visualize the model.

#### Mapping

Maps mechanical object to visualized one. There are [several](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/mappings/) ones. We use BarycentricMapping.

### Collision Node

#### Mesh Loader

There is one mesh loader for every 3d model extension (e.g. stl, obj, ...).

#### MeshTopology

The MeshTopology component defines how to represent the mesh of an object in space.

#### Mechanical object

The MechanicalObject component is needed to specify any collision model.

#### Mapping

Maps mechanical object to collision model. There are [several](https://www.sofa-framework.org/community/doc/simulation-principles/multi-model-representation/mappings/) ones. We use BarycentricMapping.

#### Collision Model

There are 3 main types of collision models:

* PointCollisionModel
* LineCollisionModel
* TriangleCollisionModel

Anyone of these can be used to define the collision model of any object.

## Other

### Monitor

The Monitor is a component used to inspect physical quantities of the simulation and can be attached to any body.  
We are willing to attach a monitor to the needle's tip and to graph the force it perceives, like if there was a force/torque sensor attached on the tip.

### FixedConstraints

In order to consider the phantom's body as fixed we applied a number of fixed constraints on some of its phases, this way it won't translate due to the force exerted by the needle.
