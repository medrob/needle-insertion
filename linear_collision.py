import Sofa
import os

class MainScene(Sofa.PythonScriptController):

	#==========================
    # Fat node
    #==========================
	def fatNode(self, meshFile_fat = 'data/phantom/Segmentation_Mar09_Fat.stl'):
    
		fatNode = self.rootNode.createChild('Fat')

		fatNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
		fatNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=True)
		fatNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', 
							dx="0", dy="0", dz="0", rx="0", ry="0", rz="0", scale="1.0")
		fatNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="10")

		fatNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-155", xmax="125", ymin="-170", ymax="45", zmin="-120", zmax="10")

		#fatNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_tissue', 
		# 						method='polar', poissonRatio="0.45", youngModulus="450", computeVonMisesStress="1", showVonMisesStressPerNode=False, 
		# 						listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)

		fatNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large', poissonRatio="0.48", youngModulus="500")

		fatNode.createObject('UncoupledConstraintCorrection')

		## Define indices for fixed constaints (needed to make sure the fat will act like if being pushed towards a wall
		indices = ''
		for k in range(10):
			for i in range(7):
				for j in range(10):
					idx = k*100 + i*10 + j
					indices += str(idx) + " "
		fatNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)


		# Visual node
		fatVisNode = fatNode.createChild('Visual')

		fatVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=meshFile_fat)
		fatVisNode.createObject('OglModel', src='@meshLoader', color='#ecc854', name='Fat')
		fatVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@Fat')

		# Collision node

		fatColNode = fatNode.createChild('Collision')

		fatColNode.createObject('MeshSTLLoader', name='meshLoader', filename=meshFile_fat)
		fatColNode.createObject('MeshTopology', src='@meshLoader')
		fatColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject', template='Vec3d', scale="1.0")
		fatColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

		fatColNode.createObject('TriangleCollisionModel', template='Vec3d')
		#fatColNode.createObject('LineCollisionModel')
		#fatColNode.createObject('PointCollisionModel')


	#==========================
	# Muscle node
	#==========================                    
	def muscleNode(self, meshFile_muscle = 'data/phantom/Segmentation_Mar09_muscle.stl'):

		muscleNode = self.rootNode.createChild('Muscle')

		muscleNode.createObject('EulerImplicitSolver', rayleighStiffness="0.1", rayleighMass="0.1")
		muscleNode.createObject('CGLinearSolver', iterations="25", tolerance="1e-9", threshold="1e-9", printLog=True)
		muscleNode.createObject('MechanicalObject', name='mechObject', template='Vec3d', dx="0", dy="-2", dz="0", rx="0", ry="0", rz="0", scale="0.99")
		muscleNode.createObject('UniformMass', template='Vec3d,double', name='mass', totalMass="30")
		muscleNode.createObject('RegularGridTopology', nx="10", ny="10", nz="10", xmin="-140", xmax="120", ymin="-155", ymax="33", zmin="-115", zmax="5")

		#muscleNode.createObject('TetrahedronFEMForceField', template='Vec3d', name='FEM_muscle', 
		# 							method='polar', poissonRatio="0.25", youngModulus="500", computeVonMisesStress="1", showVonMisesStressPerNode=False, 
		# 							listening="1", updateStiffness=False, showStressColorMap='blue 1 0 0 1  1 0.5 0.5 1', isCompliance=False)
		muscleNode.createObject('HexahedronFEMForceField', template='Vec3d', name='FEM_tissue', method='large', poissonRatio="0.45", youngModulus="450")
		
		## Define indices for fixed constaints (needed to make sure the muscle will act like if being pushed towards a wall
		indices = ''
		for k in range(10):
			for i in range(7):
				for j in range(10):
					idx = k*100 + i*10 + j
					indices += str(idx) + " "
		muscleNode.createObject('FixedConstraint', name='FixedConstraint', indices=indices)

		muscleNode.createObject('UncoupledConstraintCorrection')

		# Visual node
		muscleVisNode = muscleNode.createChild('Visual')

		muscleVisNode.createObject('MeshSTLLoader', name='meshLoader', filename=meshFile_muscle)
		muscleVisNode.createObject('OglModel', src='@meshLoader', color='#db8063', scale="0.99", name='Muscle')
		muscleVisNode.createObject('BarycentricMapping', name='Visual Mapping', output='@Muscle')

		# Collision node
		muscleColNode = muscleNode.createChild('Collision')

		muscleColNode.createObject('MeshSTLLoader', name='meshLoader', scale="0.99", filename=meshFile_muscle)
		muscleColNode.createObject('MeshTopology', src='@meshLoader')
		muscleColNode.createObject('MechanicalObject', src='@meshLoader', name='CollisionObject', template='Vec3d', scale="0.99")
		muscleColNode.createObject('BarycentricMapping', name='Mechanical Mapping')

		muscleColNode.createObject('TriangleCollisionModel', contactStiffness="10",template='Vec3d')
		#muscleColNode.createObject('LineCollisionModel')
		#muscleColNode.createObject('PointCollisionModel')


	# ==========================
	# Needle node
	# ==========================
	def needleNode(self, meshFile_needle = 'data/syrette2.obj', scale=10):

		needleNode = self.rootNode.createChild('Needle')

		needleNode.createObject('EulerImplicitSolver', name='ODE solver', rayleighStiffness="0.01", rayleighMass="1.0")
		needleNode.createObject('CGLinearSolver', name='linear solver', iterations="25", tolerance="1e-7", threshold="1e-7")
		needleNode.createObject('MechanicalObject', name='mechObject', template='Rigid3d', 
								dx="0", dy="170", dz="-50", rx="0", ry="0", rz="-90.0", scale3d=[1.2*scale, scale, scale])
		needleNode.createObject('UniformMass', name='mass', totalMass="1")
		needleNode.createObject('UncoupledConstraintCorrection')

		# Visual node
		needleVisNode = needleNode.createChild('VisualModel')

		needleVisNode.createObject('MeshObjLoader', name='instrumentMeshLoader', filename=meshFile_needle)
		needleVisNode.createObject('OglModel', name='InstrumentVisualModel', src='@instrumentMeshLoader', 
									dy="0", scale3d=[1.2*scale, scale, scale])
		needleVisNode.createObject('RigidMapping', name='MM-VM mapping', input='@../mechObject', output='@InstrumentVisualModel')

		# Collision node
		needleColNode = needleNode.createChild('CollisionModel')

		needleColNode.createObject('MeshObjLoader', filename=meshFile_needle, name='loader')
		needleColNode.createObject('MeshTopology', src='@loader', name='InstrumentCollisionModel')
		needleColNode.createObject('MechanicalObject', src='@InstrumentCollisionModel', name='instrumentCollisionState', 
									dy="0", scale3d=[1.2*scale, scale, scale])
		needleColNode.createObject('RigidMapping', name='MM-CM mapping', input='@../mechObject', output='@instrumentCollisionState')

		#needleColNode.createObject('TriangleCollisionModel', name='instrumentTriangle', contactStiffness=10, contactFriction=10)
		#needleColNode.createObject('LineCollisionModel', name='instrumentLine', contactStiffness=10, contactFriction=10)
		needleColNode.createObject('PointCollisionModel', name='instrumentPoint', contactStiffness="10", contactFriction="10")


	# ==========================
	# Root node
	# ==========================
	def createGraph(self, node):

		#displayFlags = ['showForceFields', 'showCollisionModels', 'showBehavior']
		displayFlags = []

		self.rootNode = node.getRoot()
		self.rootNode.dt = 0.005 # In seconds
		self.rootNode.gravity = [0, 0, 0]

		self.rootNode.createObject('RequiredPlugin', name='SofaMiscCollision')
		self.rootNode.createObject('RequiredPlugin', name='SofaPython')
		self.rootNode.createObject('RequiredPlugin', name='CImgPlugin')
		self.rootNode.createObject('RequiredPlugin', name='SofaOpenglVisual')
		self.rootNode.createObject('RequiredPlugin', name='SofaValidation')

		self.rootNode.createObject('VisualStyle', displayFlags=displayFlags)

		self.rootNode.createObject('FreeMotionAnimationLoop')
		self.rootNode.createObject('GenericConstraintSolver', maxIterations="50", tolerance="1e-07")
		self.rootNode.createObject('DefaultPipeline', verbose="0", depth="6", draw="1")
		self.rootNode.createObject('BruteForceDetection', name='N2')
		self.rootNode.createObject('LocalMinDistance', name='Intersection', 
									alarmDistance="0.9", contactDistance="0.6", useLMDFilters="0")
		self.rootNode.createObject('DefaultContactManager', name='Response', response='FrictionContact', responseParams='mu=10')

		# Add phantom's nodes
		self.fatNode()						# Fat node
		self.muscleNode()					# Muscle Node

		# Add needle's node
		self.needleNode(meshFile_needle="data/plug.obj", scale=4)

		# Define needle's Trajectory
		self.rootNode.getChild('Needle').createObject('LinearMovementConstraint', template='Rigid3d',
				             indices=0, keyTimes=[0, 0.8, 1.6, 1.7], movements=[[0,   0, 0, 0, 0, 0],
				                        										[0, -85, 0, 0, 0, 0],
				                        										[0,   0, 0, 0, 0, 0],
				                        										[0,   0, 0, 0, 0, 0]])


		self.rootNode.getChild('Needle').createObject('PythonScriptController', filename="controller.py", classname="controller")

		# Add monitor component to needle in order to obtain graphical representation of the forces acting on it
		#self.rootNode.getChild('Needle').createObject('ExtraMonitor', name='forces_45', template='Vec3d', indices="45",
		#											listening="1", showForces=True, ForcesColor='0 1 1 1', ExportForces=True, 
		#											resultantF=True, sizeFactor="0.1")

		return 0


def createScene(rootNode):
    obj = MainScene(rootNode)
    obj.createGraph(rootNode)
